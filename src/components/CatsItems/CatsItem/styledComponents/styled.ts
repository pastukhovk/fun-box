import styled from 'styled-components'

export interface CardItemStyleType {
    state: 'default' | 'selected' | 'selectedHover' | 'disabled'
}

type isStockType = {
    isStock: boolean
}

export const CardItemStyle = styled.div<CardItemStyleType & isStockType>`
max-width: 320px;
    height: 480px;
    text-align: left;
    position: relative;
    box-sizing: border-box;
    transition: .2s;
    
    .card__size {
        background-color:${props => (!props.isStock) ? "#b3b3b3" : (props.state === 'default') ? "#1698d9" : "#d91667"}; 
       
      }
    
    .card__buy {
    color: ${props => (props.isStock) ?"#fff" :"#ffff66" }
    }

    &:before {
      position: absolute;
      z-index: 1;
      top: 0;
      left: 0;
      width: 100%;
      height: 100%;
      content: '';
      transform: scale(1.02, 1.015);
      background-image: linear-gradient(135deg, transparent 5%, ${props => (!props.isStock) ? "#b3b3b3" : (props.state === 'default') ? "#1698d9" : "#d91667"} 0);
      border-radius: 0 10px 10px 10px;
      transition: .2s;
    }


    &-inner {
      padding: 15px 48px 48px;
      background-image: url("../../../assets/images/funbox_cats.png");
      background-repeat: no-repeat;
      box-sizing: border-box;
      background-position: 0% 101%;
      position: relative;
      z-index: 2;
      display: block;
      width: 320px;
      height: 480px;
      border-radius: 0 10px 10px 10px;

    }
`
export const CardItemMain = styled.section<CardItemStyleType & isStockType>`
background-image: linear-gradient(135deg, transparent 5%, #f2f2f2 0);
    position: relative;
    z-index: 4;
    border-radius: 0 10px 10px 10px;
    display: block;
    width: 320px;
    height: 480px;
    overflow: hidden;
    cursor:${props => (props.isStock) && 'pointer'};
    opacity:${props => (!props.isStock) && .5};
    &:hover {
      .card__size {
        background-color:${props => (!props.isStock) ? "#b3b3b3" : (props.state === 'default') ? "#2ea8e6" : (props.state === 'disabled') ? "#b3b3b3" : "#e62e7a"}; 
       
      }
      &:before {
        background-image: linear-gradient(135deg, transparent 5%, ${props => (!props.isStock) ? "#b3b3b3" : (props.state === 'default') ? "#2ea8e6" : "#e62e7a"} 0);
        transition: .2s;
      }
    }
`
export const CardItemContainer = styled.div`
width:calc(33.333% - 80px);
margin:40px;
     @media (max-width: 1180px) {
     width:calc(50% - 80px);
    display: flex;
    justify-content: center;
  }
    @media (max-width: 700px){
    width:calc(100% - 20px);
    margin:25px;
    }
    `
export const BuyLink = styled.a`
color:#107cb2;
&:hover {
color:#2ea8e6;
transition:.2s;
}

`
export const cardItemsStyle = styled.div`
display:flex;
`