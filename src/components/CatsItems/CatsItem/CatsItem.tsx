import React, {useState} from 'react';
import "./catsItem.scss"
import {BuyLink, CardItemContainer, CardItemMain, CardItemStyle, CardItemStyleType} from './styledComponents/styled'



type CatsItemPropsType = {
    foodName: string
    headerDescription: {
        default: string,
        selectedHover: string
    }
    numberOfPortions: string
    numberOfMice: string
    additionalText: string | null
    size: number
    textForBuy: {
        default: string,
        selected: string,
        disabled:string
    }
    isStock:boolean
}

export const CatsItem: React.FC<CatsItemPropsType> = (props) => {
    let initialState: CardItemStyleType = {state: "default"};
    const [state, setState] = useState(initialState)
    const onChangeState = (state: CardItemStyleType) => {
        if (!props.isStock) {
            state = {...state, state: "disabled"}
        }
        else if (state.state === "default") {
            state = {...state, state: "selectedHover"}
        } else if (state.state === "selected" || state.state === "selectedHover") {
            state = {...state, state: "default"}
        }
        return state
    }
    const onChangeStateHover = (state: CardItemStyleType) => {
        if (state.state === "selected") {
            state = {...state, state: "selectedHover"}
        } else if (state.state === "selectedHover") {
            state = {...state, state: "selected"}
        }
        return state
    }
    const onChangeStateUnHover = (state: CardItemStyleType) => {
        if (state.state === "selectedHover") {
            state = {...state, state: "selected"}
        }
        return state
    }
    const BuyMe = (state: CardItemStyleType) => {
        return state.state === 'default' ? <span>{props.textForBuy.default} <BuyLink onClick={() => {
            setState(onChangeState(state))
        }}>купи</BuyLink></span> : <span>
            {props.textForBuy.selected}
        </span>
    }
    return (
        <CardItemContainer>

            <CardItemStyle isStock = {props.isStock} state={state.state}>

                <CardItemMain isStock = {props.isStock} state={state.state}

                              onMouseEnter={() => {
                    setState(onChangeStateHover(state))
                }}
                              onMouseLeave={() => {
                                  setState(onChangeStateUnHover(state))
                              }}
                              onClick={() => {
                                  setState(onChangeState(state))
                              }} className="card__main">
                    <div className="card__item-inner">
                        <div className="card__header">
                            {state.state === "selectedHover" ? props.headerDescription.selectedHover : props.headerDescription.default}
                        </div>
                        <h2 className='card__title'>Нямушка</h2>
                        <h3 className='card__foodname'>{props.foodName}</h3>
                        <div className="card__other-info">

                            <p>{props.numberOfPortions}
                                <br/>{props.numberOfMice}<br/>{props.additionalText ? props.additionalText : null}</p>
                        </div>
                        <div className="card__size">
                            <div className='card__size-number t_center'>{props.size}</div>
                            <div className='card__size-units t_center'>кг</div>
                        </div>
                    </div>

                </CardItemMain>

                <div
                    className="card__buy">{props.isStock?BuyMe(state):props.textForBuy.disabled}</div>

            </CardItemStyle>
        </CardItemContainer>


    )
}