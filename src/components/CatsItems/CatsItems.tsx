import React, {useContext} from "react";
import {CatsItem} from "./CatsItem/CatsItem";
import {CatsItemsContext} from "../../state_management/catsContext";
import "./catsItems.scss"


export const CatsItems = () => {
    const CatsItemsData = useContext(CatsItemsContext);
    const catFoodCard = CatsItemsData.map(item =>
        <CatsItem key={item.id} foodName={item.foodName}
                  numberOfPortions={item.numberOfPortions}
                  numberOfMice={item.numberOfMice}
                  additionalText={item.additionalText}
                  size={item.size} textForBuy={item.textForBuy}
                  headerDescription={item.headerDescription}
                  isStock={item.isStock}
        />)
    return (
        <>
            <div className="cards__title_inner">
                <h2 className='cards__title'>Ты сегодня покормил кота?</h2>
            </div>
            <div className={'card__items'}>
                {catFoodCard}
            </div>

        </>
    )
}