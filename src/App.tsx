import React from 'react';
import './App.css';
import './assets/fonts/Exo2.0-Thin.otf'
import {CatsItems} from "./components/CatsItems/CatsItems";
import {CatsFoodCardsState} from "./state_management/cats-state";

function App() {
    return (
        <CatsFoodCardsState>
            <div className="App">
                <div className="container">
                <CatsItems />
                </div>
            </div>
        </CatsFoodCardsState>
    );
}

export default App;
