import {InitialStateType} from "./cats-state";


export const getCatsItemTextForBuy = (initialState: InitialStateType) => {
    initialState.CatsItemsData.forEach(item => {
        item.textForBuy.default = item.textForBuy.default.replace('купи.', "")
    })
    return initialState.CatsItemsData
}