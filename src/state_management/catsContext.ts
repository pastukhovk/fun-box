import {createContext} from 'react'
import {initialState} from "./cats-state";

export const CatsItemsContext = createContext(initialState.CatsItemsData);