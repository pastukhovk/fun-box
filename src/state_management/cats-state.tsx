import React from "react";
import {CatsItemsContext} from "./catsContext";
import {getCatsItemTextForBuy} from "./catsSelectors";

export type CatsItemDataType = {
    id: number
    headerDescription: {
        default:string,
        selectedHover:string
    }
    foodName: string
    numberOfPortions: string
    numberOfMice: string
    additionalText: string | null
    size:number
    textForBuy: {
        default:string,
        selected:string,
        disabled:string
    }
    isStock:boolean
}

export type InitialStateType = {
    CatsItemsData: Array<CatsItemDataType>
}

export const initialState: InitialStateType = {
    CatsItemsData: [
        {
            id: 1,
            headerDescription:{
                default:'Сказочное заморское яство',
                selectedHover:'Котэ не одобряет?'
            },
            foodName: 'с фуагра',
            numberOfPortions: '10 порций',
            numberOfMice: 'мышь в подарок',
            additionalText: null,
            size:0.5,
            textForBuy: {
                default:'Чего сидишь? Порадуй котэ, купи.',
                selected:'Печень утки молодая с артишоками',
                disabled:'Печалька, с фуа-гра закончился.'
            },
            isStock:true
        },
        {
            id: 2,
            headerDescription:{
                default:'Сказочное заморское яство',
                selectedHover:'Котэ не одобряет?'
            },
            foodName: 'с рыбой',
            numberOfPortions: '40 порций',
            numberOfMice: '2 мыши в подарок',
            additionalText: null,
            size:2,
            textForBuy: {
                default:'Чего сидишь? Порадуй котэ, купи.',
                selected:'Головы щучьи с чесноком да свежайшая сёмгушка.',
                disabled:'Печалька, с рыбой закончился.'
            },
            isStock:true
        },
        {
            id: 3,
            headerDescription:{
                default:'Сказочное заморское яство',
                selectedHover:'Котэ не одобряет?'
            },
            foodName: 'с курой',
            numberOfPortions: '100 порций',
            numberOfMice: '5 мышей в подарок',
            additionalText: 'заказчик доволен',
            size:5,
            textForBuy: {
                default:'Чего сидишь? Порадуй котэ, купи.',
                selected:'Филе из цыплят с трюфелями в бульоне.',
                disabled:'Печалька, с курой закончился.'
            },
            isStock:false
        }
    ]
}
type PropsType = {
    children: JSX.Element;
}
export const CatsFoodCardsState = ({children}: PropsType) => {
    const CatsItemsValue = initialState.CatsItemsData;
    const textForBuy = getCatsItemTextForBuy(initialState)
    return (
        <CatsItemsContext.Provider value={CatsItemsValue}  >
            {children}
        </CatsItemsContext.Provider>
    )
}